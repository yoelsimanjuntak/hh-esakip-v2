<?=form_open(current_url(),array('role'=>'form','id'=>'form-program', 'class'=>'form-horizontal'))?>
<div class="form-group">
  <label class="control-label col-sm-3">Bidang</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_BID?>" class="form-control" style="width: 100%" <?=!empty($dpa)?'disabled':''?> required>
      <?=GetCombobox("select * from sakip_mbid where Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub order by Nm_Bid", COL_KD_BID, COL_NM_BID, (!empty($data)?$data[COL_KD_BID]:null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Kode Program</label>
  <div class="col-sm-4">
    <input type="number" placeholder="Kode" class="form-control" name="<?=COL_KD_PROGRAMOPD?>" value="<?=!empty($data)?$data[COL_KD_PROGRAMOPD]:''?>" <?=!empty($dpa)?'disabled':''?>  required />
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Nama Program</label>
  <div class="col-sm-8">
    <input type="text" placeholder="Nama" class="form-control" name="<?=COL_NM_PROGRAMOPD?>" value="<?=!empty($data)?$data[COL_NM_PROGRAMOPD]:''?>" <?=!empty($dpa)?'disabled':''?> required />
  </div>
</div>
<?php
if(empty($dpa)) {
  ?>
  <div class="form-group">
    <label class="control-label col-sm-3">Catatan</label>
    <div class="col-sm-8">
      <textarea placeholder="Catatan" class="form-control" name="<?=COL_REMARKS?>"><?=!empty($data)?$data[COL_REMARKS]:''?></textarea>
    </div>
  </div>
  <?php
}
?>
<?php
if(!empty($rdpa)) {
  ?>
  <div class="form-group">
    <div class="col-sm-8 col-sm-offset-3">
      <p class="help-block">Program ini sudah diinput ke DPA. Mengubah informasi pada program ini akan <strong>otomatis mengubah</strong> informasi pada DPA.</p>
    </div>
  </div>
  <?php
}
?>
<?php
if(!empty($dpa)) {
  ?>
  <input type="hidden" value="1" name="<?=COL_IS_RENJA?>" />
  <div class="form-group">
    <div class="col-sm-8 col-sm-offset-3">
      <p class="help-block">Klik tombol "<strong>SUBMIT</strong>" untuk memasukkan data ini ke DPA.</p>
    </div>
  </div>
  <?php
}
?>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12 text-center">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>&nbsp;
    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;SUBMIT</button>
  </div>
</div>
<?=form_close()?>
