<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      $noaction ? anchor('ajbk/jabatan/edit/'.$d[COL_KD_JABATAN],'<i class="fa fa-eye text-primary"></i>', array('target'=>'_blank')) : anchor('ajbk/jabatan/edit/'.$d[COL_KD_JABATAN],'<i class="fa fa-clipboard text-primary"></i>').'&nbsp;&nbsp;'.anchor('ajbk/jabatan/cetak/'.$d[COL_KD_JABATAN],'<i class="fa fa-print text-success"></i>', array('target'=>'_blank')).'&nbsp;&nbsp;'.anchor('ajbk/jabatan/delete/'.$d[COL_KD_JABATAN],'<i class="fa fa-trash text-danger"></i>', array('class'=>'cekboxaction')),
      $d[COL_NM_JABATAN],
      $d[COL_ID_JABATAN],
      $d[COL_KD_TYPE]==JABATAN_TYPE_STRUKTURAL?'Struktural':($d[COL_KD_TYPE]==JABATAN_TYPE_FUNGSIONAL?'Fungsional':'??'),
      '<p class="text-sm mb-0">'.
      'Pratama : <strong>'.$d[COL_NM_SUB_UNIT].'</strong><br  />'.
      'Administrator : <strong>'.($d[COL_NM_BID]?$d[COL_NM_BID]:'-').'</strong><br  />'.
      'Pengawas : <strong>'.($d[COL_NM_SUBBID]?$d[COL_NM_SUBBID]:'-').'</strong>'.
      '</p>',
      number_format($d["Uraian"], 0),
      number_format($d["Beban"], 0),
      number_format($d["Pegawai"], 2)
    );
    $i++;
}
$data = json_encode($res);
 ?>
 <form id="form-partial" method="post" action="#">
   <table id="list-partial" class="table table-bordered table-hover">

   </table>
 </form>
 <script type="text/javascript">
 $(document).ready(function() {
     var dataTable = $('#list-partial').dataTable({
       "autoWidth" : false,
       "aaData": <?=$data?>,
       "scrollY" : '40vh',
       "scrollX": "120%",
       "iDisplayLength": 100,
       "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
       "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
       "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
       "ordering": false,
       "columnDefs":[
         {targets: [6,7], className:'text-right'},
         {targets: [4], className:'nowrap'},
         {targets: [5], className:'text-center'}
       ],
       "aoColumns": [
          {"sTitle": "#","width": "60px"},
          {"sTitle": "Jabatan"},
          {"sTitle": "Kode"},
          {"sTitle": "Tipe"},
          {"sTitle": "Unit Kerja"},
          {"sTitle": "Tugas"},
          {"sTitle": "Beban <small>(Jam / Tahun)</small>"},
          {"sTitle": "Kebutuhan Pegawai"}
       ]
     });
     $('.cekboxaction', $('#datalist')).click(function(){
         var a = $(this);
         var confirmDialog = $("#confirmDialog");

         confirmDialog.on("hidden.bs.modal", function(){
             $(".modal-body", confirmDialog).html("");
         });

         $(".modal-body", confirmDialog).html('Yakin ingin menghapus informasi jabatan ini?<br  />NB : <span class="text-danger">Data bezetting yang sudah diinput atas jabatan ini akan ikut terhapus.</span>');
         confirmDialog.modal("show");
         $(".btn-ok", confirmDialog).unbind('click').click(function() {
           var thisBtn = $(this);
             thisBtn.html("Loading...").attr("disabled", true);
             $('#dataform').ajaxSubmit({
                 dataType: 'json',
                 url : a.attr('href'),
                 success : function(data){
                     if(data.error==0){
                         RefreshData();
                     }else{
                         toastr.error(data.error);
                     }
                 },
                 error : function() {
                   toastr.error('Server error.');
                 },
                 complete: function(){
                     thisBtn.html("OK").attr("disabled", false);
                     confirmDialog.modal("hide");
                 }
             });
         });
         return false;
     });
 });
 </script>
