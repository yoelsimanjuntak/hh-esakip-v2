<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 11/27/2019
 * Time: 10:22 PM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        $i+1,
        //$d["Text"]
        '<input type="hidden" name="'.COL_KD_TUJUANOPD.'" value="'.$d[COL_KD_TUJUANOPD].'" /><input type="hidden" name="'.COL_NM_TUJUANOPD.'" value="'.$d[COL_NM_TUJUANOPD].'" />'.$d[COL_KD_TUJUANOPD].". ".$d[COL_NM_TUJUANOPD],
        '<input type="hidden" name="'.COL_KD_INDIKATORTUJUANOPD.'" value="'.$d[COL_KD_INDIKATORTUJUANOPD].'" /><input type="hidden" name="'.COL_NM_INDIKATORTUJUANOPD.'" value="'.$d[COL_NM_INDIKATORTUJUANOPD].'" />'.$d[COL_KD_TUJUANOPD].".".$d[COL_KD_INDIKATORTUJUANOPD].". ".$d[COL_NM_INDIKATORTUJUANOPD],
    );
    $i++;
}
$data = json_encode($res);
?>
<style>
    table.table-picker > tbody > tr > td {
        cursor: pointer;
    }
</style>
<form id="dataform" method="post" action="#">
    <input type="hidden" name="selID" />
    <input type="hidden" name="selText" />
    <table id="browseGrid" class="table table-bordered table-hover table-picker" width="100%" cellspacing="0">

    </table>
</form>

<script>
    $(document).ready(function () {
        var dataTable = $('#browseGrid').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            "aaSorting" : [[0,'asc']],
            //"scrollY" : 400,
            //"scrollX": "200%",
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 100, -1], [10, 100, "Semua"]],
            //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
            "aoColumns": [
                {"sTitle": "#","sWidth":"2%"},
                {"sTitle": "Tujuan"},
                {"sTitle": "Indikator Tujuan"}
            ],
            "createdRow": function (row, data, index) {
                //var elBrowseTable = $("#browseGrid>tbody");
                $(row).dblclick(function () {
                    var kdTujuanOPD = $(row).find("[name=Kd_TujuanOPD][type=hidden]").first();
                    var nmTujuanOPD = $(row).find("[name=Nm_TujuanOPD][type=hidden]").first();
                    var kdIkTujuanOPD = $(row).find("[name=Kd_IndikatorTujuanOPD][type=hidden]").first();
                    var nmIkTujuanOPD = $(row).find("[name=Nm_IndikatorTujuanOPD][type=hidden]").first();

                    $("[name=selID][type=hidden]").val(kdTujuanOPD.val()+"|"+kdIkTujuanOPD.val()).change();
                    $("[name=selText][type=hidden]").val(nmTujuanOPD.val()+"|"+nmIkTujuanOPD.val()).change();
                    $(row).closest(".modal").find("button[data-dismiss=modal]").click();
                    $(row).closest(".modal-body").empty();
                });
            }
        });
    });
</script>
<div class="clearfix"></div>