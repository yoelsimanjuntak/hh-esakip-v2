<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 8/9/2019
 * Time: 5:01 PM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - Laporan DPA.xls");
}
?>
<div class="table-responsive">
    <?php
    if($no==1) {
        ?>
        <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
            <caption>
                <h5 style="text-decoration: underline"><?="1. PERBANDINGAN TARGET DAN REALISASI KINERJA TAHUN ".$data[COL_KD_TAHUN]?></h5>
            </caption>
            <thead>
            <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Indikator Kinerja / Sasaran</th>
                <th colspan="4">Tahun <?=$data[COL_KD_TAHUN]?></th>
            </tr>
            <tr>
                <th>Satuan</th>
                <th>Target</th>
                <th>Realisasi</th>
                <th>Capaian</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $counterSasaran = 1;
            foreach($sasaran as $s) {
                ?>
                <tr>
                    <td><?=$counterSasaran?></td>
                    <td colspan="5"><strong>Sasaran</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="5"><?=$s[COL_NM_SASARANOPD]?></td>
                </tr>
                <?php
                $iksasaran = $this->db
                    ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $s[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $s[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_IKSASARAN)
                    ->result_array();
                foreach($iksasaran as $iks) {
                    ?>
                    <tr>
                        <td></td>
                        <td colspan="5"><strong>Indikator Sasaran</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="5"><?=$iks[COL_NM_INDIKATORSASARANOPD]?></td>
                    </tr>
                    <?php
                    $program = $this->db
                        ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                        ->where(COL_KD_URUSAN, $iks[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $iks[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $iks[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $iks[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $iks[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $iks[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $iks[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $iks[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $iks[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $iks[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $iks[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $iks[COL_KD_SASARANOPD])
                        ->where(COL_KD_INDIKATORSASARANOPD, $iks[COL_KD_INDIKATORSASARANOPD])
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc')
                        ->get(TBL_SAKIP_DPA_PROGRAM)
                        ->result_array();
                    foreach($program as $prg) {
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="5"><strong>Program</strong></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="5"><?=$prg[COL_NM_PROGRAMOPD]?></td>
                        </tr>
                        <?php
                        $sasaranprogram = $this->db
                            ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                            ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                            ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                            ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                            ->where(COL_KD_SUB, $prg[COL_KD_SUB])

                            ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                            ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                            ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                            ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                            ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                            ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                            ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                            ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                            ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                            ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                            ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                            ->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)
                            ->result_array();
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="5"><strong>Indikator Program</strong></td>
                        </tr>
                        <?php
                        foreach($sasaranprogram as $sprogram) {
                            $iksprogram = $this->db
                                ->where(COL_KD_TAHUN, $sprogram[COL_KD_TAHUN])
                                ->where(COL_KD_URUSAN, $sprogram[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $sprogram[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $sprogram[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $sprogram[COL_KD_SUB])

                                ->where(COL_KD_PEMDA, $sprogram[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $sprogram[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $sprogram[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $sprogram[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $sprogram[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $sprogram[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $sprogram[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $sprogram[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $sprogram[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $sprogram[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_PROGRAMOPD, $sprogram[COL_KD_PROGRAMOPD])
                                ->where(COL_KD_SASARANPROGRAMOPD, $sprogram[COL_KD_SASARANPROGRAMOPD])
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_TUJUANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                ->result_array();
                            foreach($iksprogram as $iksp) {
                                $cpprogram = (($iksp[COL_KINERJA_TW1]+$iksp[COL_KINERJA_TW2]+$iksp[COL_KINERJA_TW3]+$iksp[COL_KINERJA_TW4]) / $iksp[COL_TARGET]) * 100;
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?=$iksp[COL_NM_INDIKATORPROGRAMOPD]?></td>
                                    <td><?=$iksp[COL_KD_SATUAN]?></td>
                                    <td style="text-align: right"><?=number_format($iksp[COL_TARGET], 2)?></td>
                                    <td style="text-align: right"><?=number_format($iksp[COL_KINERJA_TW1]+$iksp[COL_KINERJA_TW2]+$iksp[COL_KINERJA_TW3]+$iksp[COL_KINERJA_TW4], 2)?></td>
                                    <td style="text-align: right"><?=number_format($cpprogram, 2)?></td>
                                </tr>
                                <?php
                                $kegiatan = $this->db
                                    ->where(COL_KD_TAHUN, $iksp[COL_KD_TAHUN])
                                    ->where(COL_KD_URUSAN, $iksp[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $iksp[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $iksp[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $iksp[COL_KD_SUB])

                                    ->where(COL_KD_PEMDA, $iksp[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $iksp[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $iksp[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $iksp[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $iksp[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $iksp[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $iksp[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $iksp[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $iksp[COL_KD_SASARANOPD])
                                    ->where(COL_KD_INDIKATORSASARANOPD, $iksp[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_PROGRAMOPD, $iksp[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_SASARANPROGRAMOPD, $iksp[COL_KD_SASARANPROGRAMOPD])
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc')
                                    ->get(TBL_SAKIP_DPA_KEGIATAN)
                                    ->result_array();
                                foreach($kegiatan as $keg) {
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="5"><strong>Kegiatan</strong></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="5"><?=$keg[COL_NM_KEGIATANOPD]?></td>
                                    </tr>
                                    <?php
                                    $ikkegiatan = $this->db
                                        ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                                        ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $keg[COL_KD_SUB])

                                        ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_TUJUANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                        ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                        ->result_array();
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="5"><strong>Indikator Kegiatan</strong></td>
                                    </tr>
                                    <?php
                                    foreach($ikkegiatan as $ikk) {
                                        $cpkegiatan = (($ikk[COL_KINERJA_TW1]+$ikk[COL_KINERJA_TW2]+$ikk[COL_KINERJA_TW3]+$ikk[COL_KINERJA_TW4]) / $ikk[COL_TARGET]) * 100;
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td><?=$ikk[COL_NM_INDIKATORKEGIATANOPD]?></td>
                                            <td><?=$ikk[COL_KD_SATUAN]?></td>
                                            <td style="text-align: right"><?=number_format($ikk[COL_TARGET], 2)?></td>
                                            <td style="text-align: right"><?=number_format($ikk[COL_KINERJA_TW1]+$ikk[COL_KINERJA_TW2]+$ikk[COL_KINERJA_TW3]+$ikk[COL_KINERJA_TW4], 2)?></td>
                                            <td style="text-align: right"><?=number_format($cpkegiatan, 2)?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                            }
                        }
                    }
                    ?>

                <?php
                }
                $counterSasaran++;
            }
            ?>
            </tbody>
        </table>
        <?php
    } else if($no==2) {
        ?>
        <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
            <caption>
                <h5 style="text-decoration: underline"><?="2. PERBANDINGAN REALISASI KINERJA"?></h5>
            </caption>
            <thead>
            <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Indikator Kinerja / Sasaran</th>
                <?php
                for($i=$rpemda[COL_KD_TAHUN_FROM]+1; $i<$data[COL_KD_TAHUN]; $i++) {
                    ?>
                    <th colspan="2">Tahun <?=$i?></th>
                <?php
                }
                ?>
                <th colspan="2">Tahun <?=$data[COL_KD_TAHUN]?></th>
            </tr>
            <tr>
                <?php
                for($i=$rpemda[COL_KD_TAHUN_FROM]+1; $i<$data[COL_KD_TAHUN]; $i++) {
                    ?>
                    <th>Satuan</th>
                    <th>Realisasi</th>
                <?php
                }
                ?>
                <th>Satuan</th>
                <th>Realisasi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $counterSasaran = 1;
            foreach($sasaran as $s) {
                ?>
                <tr>
                    <td><?=$counterSasaran?></td>
                    <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><strong>Sasaran</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><?=$s[COL_NM_SASARANOPD]?></td>
                </tr>
                <?php
                $iksasaran = $this->db
                    ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $s[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $s[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_IKSASARAN)
                    ->result_array();
                foreach($iksasaran as $iks) {
                    ?>
                    <tr>
                        <td></td>
                        <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><strong>Indikator Sasaran</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><?=$iks[COL_NM_INDIKATORSASARANOPD]?></td>
                    </tr>
                    <?php
                    $program = $this->db
                        ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                        ->where(COL_KD_URUSAN, $iks[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $iks[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $iks[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $iks[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $iks[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $iks[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $iks[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $iks[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $iks[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $iks[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $iks[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $iks[COL_KD_SASARANOPD])
                        ->where(COL_KD_INDIKATORSASARANOPD, $iks[COL_KD_INDIKATORSASARANOPD])
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc')
                        ->get(TBL_SAKIP_DPA_PROGRAM)
                        ->result_array();
                    foreach($program as $prg) {
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><strong>Program</strong></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><?=$prg[COL_NM_PROGRAMOPD]?></td>
                        </tr>
                        <?php
                        $sasaranprogram = $this->db
                            ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                            ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                            ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                            ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                            ->where(COL_KD_SUB, $prg[COL_KD_SUB])

                            ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                            ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                            ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                            ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                            ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                            ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                            ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                            ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                            ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                            ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                            ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                            ->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)
                            ->result_array();
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><strong>Indikator Program</strong></td>
                        </tr>
                        <?php
                        foreach($sasaranprogram as $sprogram) {
                            $iksprogram = $this->db
                                ->where(COL_KD_TAHUN, $sprogram[COL_KD_TAHUN])
                                ->where(COL_KD_URUSAN, $sprogram[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $sprogram[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $sprogram[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $sprogram[COL_KD_SUB])

                                ->where(COL_KD_PEMDA, $sprogram[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $sprogram[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $sprogram[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $sprogram[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $sprogram[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $sprogram[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $sprogram[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $sprogram[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $sprogram[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $sprogram[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_PROGRAMOPD, $sprogram[COL_KD_PROGRAMOPD])
                                ->where(COL_KD_SASARANPROGRAMOPD, $sprogram[COL_KD_SASARANPROGRAMOPD])
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_TUJUANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                ->result_array();
                            foreach($iksprogram as $iksp) {
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?=$iksp[COL_NM_INDIKATORPROGRAMOPD]?></td>
                                    <?php
                                    for($i=$rpemda[COL_KD_TAHUN_FROM]+1; $i<$data[COL_KD_TAHUN]; $i++) {
                                        $iksp_n = $this->db
                                            ->where(COL_KD_TAHUN, $i)
                                            ->where(COL_KD_URUSAN, $iksp[COL_KD_URUSAN])
                                            ->where(COL_KD_BIDANG, $iksp[COL_KD_BIDANG])
                                            ->where(COL_KD_UNIT, $iksp[COL_KD_UNIT])
                                            ->where(COL_KD_SUB, $iksp[COL_KD_SUB])

                                            ->where(COL_KD_PEMDA, $iksp[COL_KD_PEMDA])
                                            ->where(COL_KD_MISI, $iksp[COL_KD_MISI])
                                            ->where(COL_KD_TUJUAN, $iksp[COL_KD_TUJUAN])
                                            ->where(COL_KD_INDIKATORTUJUAN, $iksp[COL_KD_INDIKATORTUJUAN])
                                            ->where(COL_KD_SASARAN, $iksp[COL_KD_SASARAN])
                                            ->where(COL_KD_INDIKATORSASARAN, $iksp[COL_KD_INDIKATORSASARAN])
                                            ->where(COL_KD_TUJUANOPD, $iksp[COL_KD_TUJUANOPD])
                                            ->where(COL_KD_INDIKATORTUJUANOPD, $iksp[COL_KD_INDIKATORTUJUANOPD])
                                            ->where(COL_KD_SASARANOPD, $iksp[COL_KD_SASARANOPD])
                                            ->where(COL_KD_INDIKATORSASARANOPD, $iksp[COL_KD_INDIKATORSASARANOPD])
                                            ->where(COL_KD_PROGRAMOPD, $iksp[COL_KD_PROGRAMOPD])
                                            ->where(COL_KD_SASARANPROGRAMOPD, $iksp[COL_KD_SASARANPROGRAMOPD])
                                            ->where(COL_KD_INDIKATORPROGRAMOPD, $iksp[COL_KD_INDIKATORPROGRAMOPD])
                                            ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                            ->row_array();
                                        ?>
                                        <td style="text-align: right"><?=!empty($iksp_n) ? $iksp_n[COL_KD_SATUAN] : "-"?></td>
                                        <td style="text-align: right"><?=!empty($iksp_n) ? number_format($iksp_n[COL_KINERJA_TW1]+$iksp_n[COL_KINERJA_TW2]+$iksp_n[COL_KINERJA_TW3]+$iksp_n[COL_KINERJA_TW4], 2) : 0?></td>
                                    <?php
                                    }
                                    ?>
                                    <td style="text-align: right"><?=$iksp[COL_KD_SATUAN]?></td>
                                    <td style="text-align: right"><?=number_format($iksp[COL_KINERJA_TW1]+$iksp[COL_KINERJA_TW2]+$iksp[COL_KINERJA_TW3]+$iksp[COL_KINERJA_TW4], 2)?></td>
                                </tr>
                                <?php
                                $kegiatan = $this->db
                                    ->where(COL_KD_TAHUN, $iksp[COL_KD_TAHUN])
                                    ->where(COL_KD_URUSAN, $iksp[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $iksp[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $iksp[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $iksp[COL_KD_SUB])

                                    ->where(COL_KD_PEMDA, $iksp[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $iksp[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $iksp[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $iksp[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $iksp[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $iksp[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $iksp[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $iksp[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $iksp[COL_KD_SASARANOPD])
                                    ->where(COL_KD_INDIKATORSASARANOPD, $iksp[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_PROGRAMOPD, $iksp[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_SASARANPROGRAMOPD, $iksp[COL_KD_SASARANPROGRAMOPD])
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc')
                                    ->get(TBL_SAKIP_DPA_KEGIATAN)
                                    ->result_array();
                                foreach($kegiatan as $keg) {
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><strong>Kegiatan</strong></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><?=$keg[COL_NM_KEGIATANOPD]?></td>
                                    </tr>
                                    <?php
                                    $ikkegiatan = $this->db
                                        ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                                        ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $keg[COL_KD_SUB])

                                        ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_TUJUANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                        ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                        ->result_array();
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="<?=1+(($data[COL_KD_TAHUN]-$rpemda[COL_KD_TAHUN_FROM])*2)?>"><strong>Indikator Kegiatan</strong></td>
                                    </tr>
                                    <?php
                                    foreach($ikkegiatan as $ikk) {
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td><?=$ikk[COL_NM_INDIKATORKEGIATANOPD]?></td>
                                            <?php
                                            for($i=$rpemda[COL_KD_TAHUN_FROM]+1; $i<$data[COL_KD_TAHUN]; $i++) {
                                                $ikk_n = $this->db
                                                    ->where(COL_KD_TAHUN, $i)
                                                    ->where(COL_KD_URUSAN, $ikk[COL_KD_URUSAN])
                                                    ->where(COL_KD_BIDANG, $ikk[COL_KD_BIDANG])
                                                    ->where(COL_KD_UNIT, $ikk[COL_KD_UNIT])
                                                    ->where(COL_KD_SUB, $ikk[COL_KD_SUB])

                                                    ->where(COL_KD_PEMDA, $ikk[COL_KD_PEMDA])
                                                    ->where(COL_KD_MISI, $ikk[COL_KD_MISI])
                                                    ->where(COL_KD_TUJUAN, $ikk[COL_KD_TUJUAN])
                                                    ->where(COL_KD_INDIKATORTUJUAN, $ikk[COL_KD_INDIKATORTUJUAN])
                                                    ->where(COL_KD_SASARAN, $ikk[COL_KD_SASARAN])
                                                    ->where(COL_KD_INDIKATORSASARAN, $ikk[COL_KD_INDIKATORSASARAN])
                                                    ->where(COL_KD_TUJUANOPD, $ikk[COL_KD_TUJUANOPD])
                                                    ->where(COL_KD_INDIKATORTUJUANOPD, $ikk[COL_KD_INDIKATORTUJUANOPD])
                                                    ->where(COL_KD_SASARANOPD, $ikk[COL_KD_SASARANOPD])
                                                    ->where(COL_KD_INDIKATORSASARANOPD, $ikk[COL_KD_INDIKATORSASARANOPD])
                                                    ->where(COL_KD_PROGRAMOPD, $ikk[COL_KD_PROGRAMOPD])
                                                    ->where(COL_KD_SASARANPROGRAMOPD, $ikk[COL_KD_SASARANPROGRAMOPD])
                                                    ->where(COL_KD_KEGIATANOPD, $ikk[COL_KD_KEGIATANOPD])
                                                    ->where(COL_KD_INDIKATORKEGIATANOPD, $ikk[COL_KD_INDIKATORKEGIATANOPD])
                                                    ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                                    ->row_array();
                                                ?>
                                                <td style="text-align: right"><?=$ikk_n ? $ikk_n[COL_KD_SATUAN] : "-"?></td>
                                                <td style="text-align: right"><?=$ikk_n ? number_format($ikk_n[COL_KINERJA_TW1]+$ikk_n[COL_KINERJA_TW2]+$ikk_n[COL_KINERJA_TW3]+$ikk_n[COL_KINERJA_TW4], 2) : 0?></td>
                                            <?php
                                            }
                                            ?>
                                            <td style="text-align: right"><?=$ikk[COL_KD_SATUAN]?></td>
                                            <td style="text-align: right"><?=number_format($ikk[COL_KINERJA_TW1]+$ikk[COL_KINERJA_TW2]+$ikk[COL_KINERJA_TW3]+$ikk[COL_KINERJA_TW4], 2)?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                            }
                        }
                    }
                    ?>

                <?php
                }
                $counterSasaran++;
            }
            ?>
            </tbody>
        </table>
        <?php
    } else if($no==3) {
        ?>
        <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
            <caption>
                <h5 style="text-decoration: underline"><?="3. PERBANDINGAN TINGKAT CAPAIAN INDIKATOR KINERJA / SASARAN TERHADAP TARGET KINERJA RENSTRA TAHUN ".$rpemda[COL_KD_TAHUN_FROM]." - ".$rpemda[COL_KD_TAHUN_TO]?></h5>
            </caption>
            <thead>
            <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Indikator Kinerja / Sasaran</th>
                <th colspan="2">Tahun <?=$data[COL_KD_TAHUN]?></th>
                <th colspan="2">Tahun <?=$rpemda[COL_KD_TAHUN_TO]?></th>
                <th rowspan="2">% Capaian Kinerja</th>
            </tr>
            <tr>
                <th>Satuan</th>
                <th>Realisasi</th>
                <th>Satuan</th>
                <th>Realisasi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $counterSasaran = 1;
            foreach($sasaran as $s) {
                ?>
                <tr>
                    <td><?=$counterSasaran?></td>
                    <td colspan="6"><strong>Sasaran</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="6"><?=$s[COL_NM_SASARANOPD]?></td>
                </tr>
                <?php
                $iksasaran = $this->db
                    ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $s[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $s[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_IKSASARAN)
                    ->result_array();
                foreach($iksasaran as $iks) {
                    ?>
                    <tr>
                        <td></td>
                        <td colspan="6"><strong>Indikator Sasaran</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="6"><?=$iks[COL_NM_INDIKATORSASARANOPD]?></td>
                    </tr>
                    <?php
                    $program = $this->db
                        ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                        ->where(COL_KD_URUSAN, $iks[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $iks[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $iks[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $iks[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $iks[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $iks[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $iks[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $iks[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $iks[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $iks[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $iks[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $iks[COL_KD_SASARANOPD])
                        ->where(COL_KD_INDIKATORSASARANOPD, $iks[COL_KD_INDIKATORSASARANOPD])
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc')
                        ->get(TBL_SAKIP_DPA_PROGRAM)
                        ->result_array();
                    foreach($program as $prg) {
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="6"><strong>Program</strong></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="6"><?=$prg[COL_NM_PROGRAMOPD]?></td>
                        </tr>
                        <?php
                        $sasaranprogram = $this->db
                            ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                            ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                            ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                            ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                            ->where(COL_KD_SUB, $prg[COL_KD_SUB])

                            ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                            ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                            ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                            ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                            ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                            ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                            ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                            ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                            ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                            ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                            ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc')
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                            ->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)
                            ->result_array();
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="6"><strong>Indikator Program</strong></td>
                        </tr>
                        <?php
                        foreach($sasaranprogram as $sprogram) {
                            $iksprogram = $this->db
                                ->where(COL_KD_TAHUN, $sprogram[COL_KD_TAHUN])
                                ->where(COL_KD_URUSAN, $sprogram[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $sprogram[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $sprogram[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $sprogram[COL_KD_SUB])

                                ->where(COL_KD_PEMDA, $sprogram[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $sprogram[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $sprogram[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $sprogram[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $sprogram[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $sprogram[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $sprogram[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $sprogram[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $sprogram[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $sprogram[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_PROGRAMOPD, $sprogram[COL_KD_PROGRAMOPD])
                                ->where(COL_KD_SASARANPROGRAMOPD, $sprogram[COL_KD_SASARANPROGRAMOPD])
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_TUJUANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                ->result_array();
                            foreach($iksprogram as $iksp) {
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?=$iksp[COL_NM_INDIKATORPROGRAMOPD]?></td>
                                    <?php
                                    $iksp_n = $this->db
                                        ->where(COL_KD_TAHUN, $rpemda[COL_KD_TAHUN_TO])
                                        ->where(COL_KD_URUSAN, $iksp[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $iksp[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $iksp[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $iksp[COL_KD_SUB])

                                        ->where(COL_KD_PEMDA, $iksp[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $iksp[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $iksp[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $iksp[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $iksp[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $iksp[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $iksp[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $iksp[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $iksp[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $iksp[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_PROGRAMOPD, $iksp[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $iksp[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_INDIKATORPROGRAMOPD, $iksp[COL_KD_INDIKATORPROGRAMOPD])
                                        ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                        ->row_array();
                                    $iksp_n_sum = $this->db
                                        ->select('SUM(Kinerja_TW1+Kinerja_TW2+Kinerja_TW3+Kinerja_TW4) as TotalKinerja')
                                        ->where(COL_KD_TAHUN." > ", $rpemda[COL_KD_TAHUN_FROM])
                                        ->where(COL_KD_TAHUN." <= ", $rpemda[COL_KD_TAHUN_TO])
                                        ->where(COL_KD_URUSAN, $iksp[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $iksp[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $iksp[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $iksp[COL_KD_SUB])

                                        ->where(COL_KD_PEMDA, $iksp[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $iksp[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $iksp[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $iksp[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $iksp[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $iksp[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $iksp[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $iksp[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $iksp[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $iksp[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_PROGRAMOPD, $iksp[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $iksp[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_INDIKATORPROGRAMOPD, $iksp[COL_KD_INDIKATORPROGRAMOPD])
                                        ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                        ->row_array();
                                    $cpprogram = !empty($iksp_n_sum) && $iksp_n_sum["TotalKinerja"] > 0 ? (($iksp[COL_KINERJA_TW1]+$iksp[COL_KINERJA_TW2]+$iksp[COL_KINERJA_TW3]+$iksp[COL_KINERJA_TW4]) / $iksp_n_sum["TotalKinerja"]) * 100 : 0;
                                    ?>
                                    <td style="text-align: right"><?=$iksp[COL_KD_SATUAN]?></td>
                                    <td style="text-align: right"><?=number_format($iksp[COL_KINERJA_TW1]+$iksp[COL_KINERJA_TW2]+$iksp[COL_KINERJA_TW3]+$iksp[COL_KINERJA_TW4], 2)?></td>
                                    <td style="text-align: right"><?=!empty($iksp_n) ? $iksp_n[COL_KD_SATUAN] : "-"?></td>
                                    <td style="text-align: right"><?=!empty($iksp_n_sum) ? number_format($iksp_n_sum["TotalKinerja"], 2) : 0?></td>
                                    <td style="text-align: right"><?=number_format($cpprogram, 2)?></td>
                                </tr>
                                <?php
                                $kegiatan = $this->db
                                    ->where(COL_KD_TAHUN, $iksp[COL_KD_TAHUN])
                                    ->where(COL_KD_URUSAN, $iksp[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $iksp[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $iksp[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $iksp[COL_KD_SUB])

                                    ->where(COL_KD_PEMDA, $iksp[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $iksp[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $iksp[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $iksp[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $iksp[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $iksp[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $iksp[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $iksp[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $iksp[COL_KD_SASARANOPD])
                                    ->where(COL_KD_INDIKATORSASARANOPD, $iksp[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_PROGRAMOPD, $iksp[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_SASARANPROGRAMOPD, $iksp[COL_KD_SASARANPROGRAMOPD])
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc')
                                    ->get(TBL_SAKIP_DPA_KEGIATAN)
                                    ->result_array();
                                foreach($kegiatan as $keg) {
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="6"><strong>Kegiatan</strong></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="6"><?=$keg[COL_NM_KEGIATANOPD]?></td>
                                    </tr>
                                    <?php
                                    $ikkegiatan = $this->db
                                        ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                                        ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $keg[COL_KD_SUB])

                                        ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_TUJUANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                        ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                        ->result_array();
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="6"><strong>Indikator Kegiatan</strong></td>
                                    </tr>
                                    <?php
                                    foreach($ikkegiatan as $ikk) {
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td><?=$ikk[COL_NM_INDIKATORKEGIATANOPD]?></td>
                                            <?php
                                            $ikk_n = $this->db
                                                ->where(COL_KD_TAHUN, $rpemda[COL_KD_TAHUN_TO])
                                                ->where(COL_KD_URUSAN, $ikk[COL_KD_URUSAN])
                                                ->where(COL_KD_BIDANG, $ikk[COL_KD_BIDANG])
                                                ->where(COL_KD_UNIT, $ikk[COL_KD_UNIT])
                                                ->where(COL_KD_SUB, $ikk[COL_KD_SUB])

                                                ->where(COL_KD_PEMDA, $ikk[COL_KD_PEMDA])
                                                ->where(COL_KD_MISI, $ikk[COL_KD_MISI])
                                                ->where(COL_KD_TUJUAN, $ikk[COL_KD_TUJUAN])
                                                ->where(COL_KD_INDIKATORTUJUAN, $ikk[COL_KD_INDIKATORTUJUAN])
                                                ->where(COL_KD_SASARAN, $ikk[COL_KD_SASARAN])
                                                ->where(COL_KD_INDIKATORSASARAN, $ikk[COL_KD_INDIKATORSASARAN])
                                                ->where(COL_KD_TUJUANOPD, $ikk[COL_KD_TUJUANOPD])
                                                ->where(COL_KD_INDIKATORTUJUANOPD, $ikk[COL_KD_INDIKATORTUJUANOPD])
                                                ->where(COL_KD_SASARANOPD, $ikk[COL_KD_SASARANOPD])
                                                ->where(COL_KD_INDIKATORSASARANOPD, $ikk[COL_KD_INDIKATORSASARANOPD])
                                                ->where(COL_KD_PROGRAMOPD, $ikk[COL_KD_PROGRAMOPD])
                                                ->where(COL_KD_SASARANPROGRAMOPD, $ikk[COL_KD_SASARANPROGRAMOPD])
                                                ->where(COL_KD_KEGIATANOPD, $ikk[COL_KD_KEGIATANOPD])
                                                ->where(COL_KD_INDIKATORKEGIATANOPD, $ikk[COL_KD_INDIKATORKEGIATANOPD])
                                                ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                                ->row_array();
                                            $ikk_n_sum = $this->db
                                                ->select('SUM(Kinerja_TW1+Kinerja_TW2+Kinerja_TW3+Kinerja_TW4) as TotalKinerja')
                                                ->where(COL_KD_TAHUN." > ", $rpemda[COL_KD_TAHUN_FROM])
                                                ->where(COL_KD_TAHUN." <= ", $rpemda[COL_KD_TAHUN_TO])
                                                ->where(COL_KD_URUSAN, $ikk[COL_KD_URUSAN])
                                                ->where(COL_KD_BIDANG, $ikk[COL_KD_BIDANG])
                                                ->where(COL_KD_UNIT, $ikk[COL_KD_UNIT])
                                                ->where(COL_KD_SUB, $ikk[COL_KD_SUB])

                                                ->where(COL_KD_PEMDA, $ikk[COL_KD_PEMDA])
                                                ->where(COL_KD_MISI, $ikk[COL_KD_MISI])
                                                ->where(COL_KD_TUJUAN, $ikk[COL_KD_TUJUAN])
                                                ->where(COL_KD_INDIKATORTUJUAN, $ikk[COL_KD_INDIKATORTUJUAN])
                                                ->where(COL_KD_SASARAN, $ikk[COL_KD_SASARAN])
                                                ->where(COL_KD_INDIKATORSASARAN, $ikk[COL_KD_INDIKATORSASARAN])
                                                ->where(COL_KD_TUJUANOPD, $ikk[COL_KD_TUJUANOPD])
                                                ->where(COL_KD_INDIKATORTUJUANOPD, $ikk[COL_KD_INDIKATORTUJUANOPD])
                                                ->where(COL_KD_SASARANOPD, $ikk[COL_KD_SASARANOPD])
                                                ->where(COL_KD_INDIKATORSASARANOPD, $ikk[COL_KD_INDIKATORSASARANOPD])
                                                ->where(COL_KD_PROGRAMOPD, $ikk[COL_KD_PROGRAMOPD])
                                                ->where(COL_KD_SASARANPROGRAMOPD, $ikk[COL_KD_SASARANPROGRAMOPD])
                                                ->where(COL_KD_KEGIATANOPD, $ikk[COL_KD_KEGIATANOPD])
                                                ->where(COL_KD_INDIKATORKEGIATANOPD, $ikk[COL_KD_INDIKATORKEGIATANOPD])
                                                ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                                ->row_array();
                                            $cpkegiatan = !empty($ikk_n_sum) && $ikk_n_sum["TotalKinerja"] > 0 ? (($ikk[COL_KINERJA_TW1]+$ikk[COL_KINERJA_TW2]+$ikk[COL_KINERJA_TW3]+$ikk[COL_KINERJA_TW4]) / $ikk_n_sum["TotalKinerja"]) * 100 : 0;
                                            ?>
                                            <td style="text-align: right"><?=$ikk[COL_KD_SATUAN]?></td>
                                            <td style="text-align: right"><?=number_format($ikk[COL_KINERJA_TW1]+$ikk[COL_KINERJA_TW2]+$ikk[COL_KINERJA_TW3]+$ikk[COL_KINERJA_TW4], 2)?></td>
                                            <td style="text-align: right"><?=!empty($ikk_n) ? $ikk_n[COL_KD_SATUAN] : "-"?></td>
                                            <td style="text-align: right"><?=!empty($ikk_n_sum) ? number_format($ikk_n_sum["TotalKinerja"], 2) : 0?></td>
                                            <td style="text-align: right"><?=number_format($cpkegiatan, 2)?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                            }
                        }
                    }
                    ?>

                <?php
                }
                $counterSasaran++;
            }
            ?>
            </tbody>
        </table>
        <?php
    }
    ?>
</div>