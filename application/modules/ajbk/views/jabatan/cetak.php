<html>
<head>
  <title>Informasi Jabatan - <?=$data['NM_JAB']?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
<table width="100%">
  <tr>
    <td colspan="2" style="text-align: center; vertical-align: middle">
      <h4 style="text-decoration: underline">INFORMASI JABATAN</h4>
    </td>
  </tr>
</table>
<br />
<table width="100%" border="0">
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">1.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">NAMA JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data['NM_JAB']?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">2.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KODE JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data[COL_ID_JABATAN]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">3.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">UNIT KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>a.</strong> JPT Pratama</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td>PEMERINTAH KABUPATEN HUMBANG HASUNDUTAN</td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>b.</strong> JPT Madya</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data[COL_NM_SUB_UNIT]?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>c.</strong> Administrator</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=($data[COL_NM_BID]?$data[COL_NM_BID]:'-')?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>d.</strong> Pengawas</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=($data[COL_NM_SUBBID]?$data[COL_NM_SUBBID]:'-')?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>e.</strong> Jabatan</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data['NM_JAB']?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">4.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">IKHTISAR JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data[COL_NM_IKHTISAR]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">5.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">SYARAT JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>a.</strong> Pendidikan</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data['NM_EDU']?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>b.</strong> Diklat</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td>
      <?php
      $htmlDiklat = '';
      $rdiklat = $this->db
      ->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])
      ->join(TBL_AJBK_OPTION,TBL_AJBK_OPTION.'.'.COL_UNIQ." = ".TBL_AJBK_JABATAN_DIKLAT.".".COL_KD_DIKLAT,"left")
      ->get(TBL_AJBK_JABATAN_DIKLAT)
      ->result_array();
      foreach($rdiklat as $s) {
        $htmlDiklat = $s[COL_OPT_NAME].'<br />';
      }
      echo $htmlDiklat;
      ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>c.</strong> Pengalaman</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data[COL_KD_PENGALAMAN]?></td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">6.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">TUGAS POKOK</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; width: 300px; font-weight: bold; text-align: center">URAIAN TUGAS</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">HASIL KERJA</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">JUMLAH<br />BEBAN<br />KERJA<br />1 TAHUN</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">WAKTU<br />SELESAI<br />(JAM)</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">WAKTU<br />EFEKTIF<br />(JAM)</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">KEBUTUHAN<br />PEGAWAI</td>
        </tr>
        <?php
        $det = $this->db
        ->select('*, unit.Opt_Name as NM_UNIT')
        ->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])
        ->join(TBL_AJBK_OPTION.' unit','unit.'.COL_UNIQ." = ".TBL_AJBK_JABATAN_URAIAN.".".COL_KD_SATUAN,"left")
        ->get(TBL_AJBK_JABATAN_URAIAN)
        ->result_array();
        $i=1;
        $sumdet = 0;
        if(empty($det)) {
          echo '<tr><td colspan="7" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($det as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top; width: 200px;"><?=$m[COL_NM_URAIAN]?></td>
            <td style="vertical-align: top;"><?=$m['NM_UNIT']?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format($m[COL_JLH_BEBAN])?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format($m[COL_JLH_JAM])?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format(($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])/HOURPERYEAR, 2)?></td>
          </tr>
          <?php
          $i++;
          $sumdet += (($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])/HOURPERYEAR);
        }
        ?>
        <tr>
          <td colspan="6" rowspan="2" style="vertical-align: middle; width: 10px; font-weight: bold; text-align: right">KEBUTUHAN</td>
          <td style="vertical-align: top; width: 10px; font-weight: bold; text-align: center"><?=number_format($sumdet, 2)?></td>
        </tr>
        <tr>
          <td style="vertical-align: top; width: 10px; font-weight: bold; text-align: center; color: blue"><?=round($sumdet)?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">7.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">BAHAN KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">BAHAN KERJA</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PENGGUNAAN</td>
        </tr>
        <?php
        $detBahan = json_decode($data[COL_NM_BAHANKERJA]);
        $i=1;
        if(empty($detBahan)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detBahan as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->BahanKerja?></td>
            <td style="vertical-align: top;"><?=$m->Kegunaan?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">8.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">PERANGKAT KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">BAHAN KERJA</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PENGGUNAAN</td>
        </tr>
        <?php
        $detPerangkat = json_decode($data[COL_NM_PERANGKATKERJA]);
        $i=1;
        if(empty($detPerangkat)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detPerangkat as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->PerangkatKerja?></td>
            <td style="vertical-align: top;"><?=$m->Kegunaan?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">9.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">TANGGUNG JAWAB</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table border="0" width="100%">
        <?php
        $detTanggungJawab = json_decode($data[COL_NM_TANGGUNGJAWAB]);
        $i=1;
        if(!empty($detTanggungJawab) && count($detTanggungJawab) > 0) {

        } else {
          foreach($detTanggungJawab as $m) {
            ?>
            <tr>
              <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?>.</td>
              <td style="vertical-align: top;"><?=$m->TanggungJawab?></td>
            </tr>
            <?php
            $i++;
          }
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">10.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">WEWENANG</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table border="0" width="100%">
        <?php
        $detWewenang = json_decode($data[COL_NM_WEWENANG]);
        $i=1;
        foreach($detWewenang as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?>.</td>
            <td style="vertical-align: top;"><?=$m->Wewenang?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">11.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KORELASI JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">JABATAN</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">UNIT KERJA / INSTANSI</td>
        </tr>
        <?php
        $detKorelasi = json_decode($data[COL_NM_KORELASI]);
        $i=1;
        if(empty($detKorelasi)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detKorelasi as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->Jabatan?></td>
            <td style="vertical-align: top;"><?=$m->Instansi?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">12.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KONDISI LINGKUNGAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <?php
      $detLingkungan = json_decode($data[COL_NM_KONDISI]);
      if(!empty($detLingkungan)) {
        ?>
        <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
          <tr>
            <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
            <td style="vertical-align: middle; font-weight: bold; text-align: center">ASPEK</td>
            <td style="vertical-align: middle; font-weight: bold; text-align: center">FAKTOR</td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">a.</td>
            <td style="vertical-align: top; width: 200px;">Tempat Kerja</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->TempatKerja)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">b.</td>
            <td style="vertical-align: top; width: 200px;">Suhu</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Suhu)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">c.</td>
            <td style="vertical-align: top; width: 200px;">Udara</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Udara)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">d.</td>
            <td style="vertical-align: top; width: 200px;">Keadaan Ruangan</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->KeadaanRuangan)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">e.</td>
            <td style="vertical-align: top; width: 200px;">Letak</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Letak)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">f.</td>
            <td style="vertical-align: top; width: 200px;">Penerangan</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Penerangan)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">g.</td>
            <td style="vertical-align: top; width: 200px;">Suara</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Suara)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">h.</td>
            <td style="vertical-align: top; width: 200px;">Getaran</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Getaran)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
        </table>
        <?php
      }
      ?>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">13.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">RESIKO BAHAYA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">ASPEK</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PENYEBAB</td>
        </tr>
        <?php
        $detResiko = json_decode($data[COL_NM_RESIKO]);
        $i=1;
        if(empty($detResiko)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detResiko as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->Aspek?></td>
            <td style="vertical-align: top;"><?=$m->Penyebab?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
</table>
</body>
