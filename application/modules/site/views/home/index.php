<style>
.todo-list>li:hover {
    background-color: #ccc;
}
</style>
<div class="content-header">
    <!--<div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>
    </div>-->
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header border-0">
            <h5 class="m-0"><?=$this->setting_web_name?>&nbsp;<small class="font-italic">(<?=$this->setting_web_desc?>)</small></h5>
          </div>
          <div class="card-body p-0">
            <?php
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, "desc");
            $rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            ?>
            <table class="table">
                <tbody>
                <tr>
                    <td style="white-space: nowrap">PERIODE</td>
                    <td style="width: 10px">:</td>
                    <td><b><?=$rperiod?$rperiod[COL_KD_TAHUN_FROM]." s.d ".$rperiod[COL_KD_TAHUN_TO]:"-"?></b></td>
                </tr>
                <tr>
                    <td style="white-space: nowrap">KEPALA DAERAH</td>
                    <td style="width: 10px">:</td>
                    <td><b><?=$rperiod?strtoupper($rperiod[COL_NM_PEJABAT]):"-"?></b></td>
                </tr>
                <tr>
                    <td style="white-space: nowrap">VISI</td>
                    <td style="width: 10px">:</td>
                    <td><b><?=$rperiod?strtoupper($rperiod[COL_NM_VISI]):"-"?></b></td>
                </tr>
                <tr>
                    <td style="white-space: nowrap">MISI</td>
                    <td style="width: 10px">:</td>
                    <td></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <ul class="todo-list ui-sortable">
                      <?php
                      if($rperiod) {
                        $rmisi = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->order_by(COL_KD_MISI, "asc")->get(TBL_SAKIP_MPMD_MISI)->result_array();
                        $i = 1;
                        foreach($rmisi as $m) {
                          ?>
                          <li class="active" style="border-left: 2px solid #3d9970">
                            <!--<small class="badge badge-warning"><?=strtoupper($m[COL_KD_MISI])?></small>-->
                            <span class="text"><?=strtoupper($m[COL_NM_MISI])?></span>
                          </li>
                          <?php
                          $i++;
                        }
                      }
                      ?>
                    </ul>
                  </td>
                </tr>
                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card card-olive card-outline">
          <div class="card-header">
            <h5 class="card-title">E-SAKIP</h5>
          </div>
          <div class="card-body p-0">
            <?php
            $countTujuanOPD = 0;
            $countSasaranOPD = 0;
            $countProgramOPD = 0;
            $countKegiatanOPD = 0;
            if(!empty($rperiod)) {
              $countTujuanOPD = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->group_by(array(COL_KD_PEMDA, COL_KD_TUJUANOPD, COL_KD_URUSAN, COL_KD_BIDANG, COL_KD_UNIT, COL_KD_SUB))->count_all_results(TBL_SAKIP_MOPD_TUJUAN);
              $countSasaranOPD = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->group_by(array(COL_KD_PEMDA, COL_KD_TUJUANOPD, COL_KD_SASARANOPD, COL_KD_URUSAN, COL_KD_BIDANG, COL_KD_UNIT, COL_KD_SUB))->count_all_results(TBL_SAKIP_MOPD_SASARAN);
              $countProgramOPD = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->where(COL_KD_TAHUN, date('Y'))->count_all_results(TBL_SAKIP_DPA_PROGRAM);
              $countKegiatanOPD = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->where(COL_KD_TAHUN, date('Y'))->count_all_results(TBL_SAKIP_DPA_KEGIATAN);
            }
            ?>
            <ul class="nav nav-pills flex-column">
              <li class="nav-item">
                <a href="javascript: void(0)" class="nav-link">Tujuan OPD <span class="badge bg-purple float-right"><?=number_format($countTujuanOPD)?></span></a>
              </li>
              <li class="nav-item">
                <a href="javascript: void(0)" class="nav-link">Sasaran OPD <span class="badge bg-indigo float-right"><?=number_format($countSasaranOPD)?></span></a>
              </li>
              <li class="nav-item">
                <a href="javascript: void(0)" class="nav-link">Program <span class="badge bg-olive float-right"><?=number_format($countProgramOPD)?></span></a>
              </li>
              <li class="nav-item">
                <a href="javascript: void(0)" class="nav-link">Kegiatan <span class="badge bg-success float-right"><?=number_format($countKegiatanOPD)?></span></a>
              </li>
            </ul>
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col-sm-12 pl-0 pr-0">
                <a href="<?=site_url('site/home/status-opd')?>" class="btn btn-outline-info btn-sm" title="Rekapitulasi Data OPD">
                  <i class="fad fa-clipboard"></i>&nbsp;REKAPITULASI
                </a>
                <a href="<?=site_url('sakip/user/login')?>" class="btn btn-outline-success btn-sm float-right">
                  <i class="fad fa-sign-in"></i>&nbsp;MASUK
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="card card-danger card-outline">
          <div class="card-header">
            <h5 class="card-title">CONTROL PANEL</h5>
          </div>
          <div class="card-body p-0">
            <?php
            $countOPD = $this->db->count_all_results(TBL_REF_SUB_UNIT);
            $countAkun = $this->db->count_all_results(TBL__USERS);
            ?>
            <ul class="nav nav-pills flex-column">
              <li class="nav-item">
                <a href="javascript: void(0)" class="nav-link">OPD <span class="badge bg-primary float-right"><?=number_format($countOPD)?></span></a>
              </li>
              <li class="nav-item">
                <a href="javascript: void(0)" class="nav-link">Akun <span class="badge bg-info float-right"><?=number_format($countAkun)?></span></a>
              </li>
            </ul>
          </div>
          <div class="card-footer text-right">
            <a href="<?=site_url('site/user/login')?>" class="btn btn-outline-danger btn-sm">
              <i class="fad fa-sign-in"></i>&nbsp;MASUK
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
