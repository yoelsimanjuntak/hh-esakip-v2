<?php
class User extends MY_Controller {
  function __construct() {
      parent::__construct();
      $this->load->library('encrypt');
      $this->load->model('muser');
      /*if(IsLogin() && GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEKADIS) {
        redirect('site/home');
      }*/
  }

  function Login(){
      if(IsLogin()) {
          redirect('ajbk/user/dashboard');
      }
      $rules = array(
          array(
              'field' => 'UserName',
              'label' => 'UserName',
              'rules' => 'required'
          ),
          array(
              'field' => 'Password',
              'label' => 'Password',
              'rules' => 'required'
          )
      );
      $this->form_validation->set_rules($rules);
      if($this->form_validation->run()){
          //$this->load->library('si/securimage');
          $username = $this->input->post(COL_USERNAME);
          $password = $this->input->post(COL_PASSWORD);

          /*if($this->securimage->check($this->input->post('Captcha')) != true){
              redirect(site_url('site/user/login')."?msg=captcha");
          }*/

          if($this->muser->authenticate($username, $password)) {
              if($this->muser->IsSuspend($username)) {
                  redirect(site_url('ajbk/user/login')."?msg=suspend");
              }

              // Update Last Login IP
              $this->db->where(COL_USERNAME, $username);
              $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

              $userdetails = $this->muser->getdetails($username);
              SetLoginSession($userdetails);
              redirect('ajbk/user/dashboard');
          }
          else {
              redirect(site_url('ajbk/user/login')."?msg=notmatch");
          }
      } else {
          $this->load->view('user/login');
      }

  }
  function Logout(){
      UnsetLoginSession();
      redirect(site_url());
  }
  function Dashboard() {
      if(!IsLogin()) {
          redirect('ajbk/user/login');
      }
      $data['user'] = GetLoggedUser();
      $data['title'] = 'Dashboard';
  		$this->template->load('backend', 'ajbk/user/dashboard', $data);
  }
  function ChangePassword() {
      if(!IsLogin()) {
          redirect('ajbk/user/login');
      }
      $user = GetLoggedUser();
      $data['title'] = 'Change Password';
      $rules = array(
          array(
              'field' => 'OldPassword',
              'label' => 'Old Password',
              'rules' => 'required'
          ),
          array(
              'field' => COL_PASSWORD,
              'label' => COL_PASSWORD,
              'rules' => 'required'
          ),
          array(
              'field' => 'RepeatPassword',
              'label' => 'Repeat Password',
              'rules' => 'required|matches[Password]'
          )
      );
      $this->form_validation->set_rules($rules);

      if($this->form_validation->run()){
          $rcheck = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->get(TBL__USERS)->row_array();
          if(!$rcheck) {
              redirect(site_url('ajbk/user/changepassword'));
          }
          if($rcheck[COL_PASSWORD] != md5($this->input->post("OldPassword"))) {
              redirect(site_url('ajbk/user/changepassword')."?nomatch=1");
          }
          $upd = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
          if($upd) redirect(site_url('ajbk/user/changepassword')."?success=1");
          else redirect(site_url('ajbk/user/changepassword')."?error=1");
      }
      else {
          $this->load->view('ajbk/changepassword', $data);
      }
  }
}
 ?>
