<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 29/09/2018
 * Time: 22:45
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mpemda/period')?>"> Periode, Visi & Misi</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN_FROM?>" value="<?= $edit ? $data[COL_KD_TAHUN_FROM] : date("Y")?>" required>
                            </div>
                            <div class="col-sm-1">
                                <p style="text-align: center; margin: 5px 0px">s.d</p>
                            </div>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN_TO?>" value="<?= $edit ? $data[COL_KD_TAHUN_TO] : date("Y")+5?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Kepala Daerah</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="<?=COL_NM_PEJABAT?>" value="<?= $edit ? $data[COL_NM_PEJABAT] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Visi</label>
                            <div class="col-sm-5">
                                <textarea class="form-control" name="<?=COL_NM_VISI?>" required><?= $edit ? $data[COL_NM_VISI] : ""?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Misi</label>
                            <div class="col-sm-8">
                                <table class="table table-bordered" id="tbl-misi">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th width="80%">Deskripsi</th>
                                        <th style="width: 40px"><button type="button" id="btn-add-misi" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="tr-blueprint-misi" style="display: none">
                                        <td><input type="text" name="text-misi-no" class="form-control" /></td>
                                        <td><input type="text" name="text-misi-desc" class="form-control" /></td>
                                        <td>
                                            <button type="button" class="btn btn-default btn-flat btn-del-misi"><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $misi = $this->db->where(COL_KD_PEMDA, ($edit?$data[COL_KD_PEMDA]:-999))->order_by(COL_KD_MISI, 'asc')->get(TBL_SAKIP_MPMD_MISI)->result_array();
                                    foreach($misi as $m) {
                                        ?>
                                        <tr>
                                            <td><input type="text" name="text-misi-no" class="form-control" value="<?=$m[COL_KD_MISI]?>" /></td>
                                            <td><input type="text" name="text-misi-desc" class="form-control" value="<?=$m[COL_NM_MISI]?>" /></td>
                                            <td>
                                                <button type="button" class="btn btn-default btn-flat btn-del-misi"><i class="fa fa-minus"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mpemda/period')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(".btn-del-misi").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-misi").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-misi").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-misi").show();
            $(".btn-del-misi", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });
        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                var misi = [];
                var rowMisi = $("#tbl-misi>tbody").find("tr:not(.tr-blueprint-misi)");

                for (var i = 0; i < rowMisi.length; i++) {
                    var noMisi = $("[name=text-misi-no]", $(rowMisi[i])).val();
                    var ketMisi = $("[name=text-misi-desc]", $(rowMisi[i])).val();
                    misi.push({ No: noMisi, Ket: ketMisi });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < misi.length; i++) {
                    var newEl = "<input type='hidden' class='appended' name=NoMisi[" + i + "] value='" + misi[i].No + "' /><input type='hidden' class='appended' name=KetMisi[" + i + "] value='" + misi[i].Ket + "' />";
                    $("#main-form").append(newEl);
                }
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        $(".modal-body", $("#alertDialog")).html(a.responseText);
                        $("#alertDialog").modal('show');
                        //alert(a.responseText);
                    }
                });
                return false;
            }
        });
    </script>
<?php $this->load->view('footer') ?>