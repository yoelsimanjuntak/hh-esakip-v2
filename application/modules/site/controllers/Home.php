<?php
class Home extends MY_Controller {

  function index() {
    $data['title'] = 'Beranda';
		$this->template->set('title', 'Home');
		$this->template->load('frontend' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function status_opd($Tahun=0) {
      $data['title'] = 'Rekapitulasi Data OPD';
      $Tahun = $Tahun!=0?$Tahun:date('Y');
      $KdPemda = 0;

      $rpemda = $this->db
      ->where(COL_KD_TAHUN_FROM." <= ", date('Y'))
      ->where(COL_KD_TAHUN_TO." >= ", date('Y'))
      ->get(TBL_SAKIP_MPEMDA)
      ->row_array();

      if(!empty($rpemda)) {
        $KdPemda = $rpemda[COL_KD_PEMDA];
      }

      $data['Tahun'] = $Tahun;
      $data['KdPemda'] = $KdPemda;

      if(!empty($_GET)) {
        $kdUrusan = $this->input->get(COL_KD_URUSAN);
        $kdBidang = $this->input->get(COL_KD_BIDANG);
        $kdUnit = $this->input->get(COL_KD_UNIT);
        $kdSub = $this->input->get(COL_KD_SUB);
        $data['kdUrusan'] = $kdUrusan;
        $data['kdBidang'] = $kdBidang;
        $data['kdUnit'] = $kdUnit;
        $data['kdSub'] = $kdSub;

        $q = @"
select
opd.*,
(
	select count(distinct tujuan.Kd_TujuanOPD) from `sakip_mopd_tujuan` tujuan
	where
		tujuan.Kd_Pemda = $KdPemda
		and tujuan.Kd_Urusan = opd.Kd_Urusan
		and tujuan.Kd_Bidang = opd.Kd_Bidang
		and tujuan.Kd_Unit = opd.Kd_Unit
		and tujuan.Kd_Sub = opd.Kd_Sub
) as Tujuan,
(
	select count(distinct sasaran.Kd_TujuanOPD, sasaran.Kd_SasaranOPD) from `sakip_mopd_sasaran` sasaran
	where
		sasaran.Kd_Pemda = $KdPemda
		and sasaran.Kd_Urusan = opd.Kd_Urusan
		and sasaran.Kd_Bidang = opd.Kd_Bidang
		and sasaran.Kd_Unit = opd.Kd_Unit
		and sasaran.Kd_Sub = opd.Kd_Sub
) as Sasaran
from ref_sub_unit opd
where
  opd.Kd_Urusan = $kdUrusan
  and opd.Kd_Bidang = $kdBidang
  and opd.Kd_Unit = $kdUnit
  and opd.Kd_Sub = $kdSub
        ";
        $data['res'] = $res = $this->db->query($q)->row_array();
        if(empty($res)) {
          redirect('site/home/status-opd');
        }

        $data['title'] = 'Rekapitulasi '.$res[COL_NM_SUB_UNIT];
        $this->template->load('frontend' , 'home/status_opd_detail', $data);
      } else {
        $q = @"
select
opd.*,
(
	select count(distinct tujuan.Kd_TujuanOPD) from `sakip_mopd_tujuan` tujuan
	where
		tujuan.Kd_Pemda = $KdPemda
		and tujuan.Kd_Urusan = opd.Kd_Urusan
		and tujuan.Kd_Bidang = opd.Kd_Bidang
		and tujuan.Kd_Unit = opd.Kd_Unit
		and tujuan.Kd_Sub = opd.Kd_Sub
) as Tujuan,
(
	select count(distinct sasaran.Kd_TujuanOPD, sasaran.Kd_SasaranOPD) from `sakip_mopd_sasaran` sasaran
	where
		sasaran.Kd_Pemda = $KdPemda
		and sasaran.Kd_Urusan = opd.Kd_Urusan
		and sasaran.Kd_Bidang = opd.Kd_Bidang
		and sasaran.Kd_Unit = opd.Kd_Unit
		and sasaran.Kd_Sub = opd.Kd_Sub
) as Sasaran,
(
	select count(*) from `sakip_mbid_program` prg
	where
		prg.Kd_Pemda = $KdPemda
		and prg.Kd_Tahun = $Tahun
		and prg.Kd_Urusan = opd.Kd_Urusan
		and prg.Kd_Bidang = opd.Kd_Bidang
		and prg.Kd_Unit = opd.Kd_Unit
		and prg.Kd_Sub = opd.Kd_Sub
) as Program,
(
	select count(*) from `sakip_msubbid_kegiatan` keg
	where
		keg.Kd_Pemda = $KdPemda
		and keg.Kd_Tahun = $Tahun
		and keg.Kd_Urusan = opd.Kd_Urusan
		and keg.Kd_Bidang = opd.Kd_Bidang
		and keg.Kd_Unit = opd.Kd_Unit
		and keg.Kd_Sub = opd.Kd_Sub
) as Kegiatan,
(
	select count(*) from `sakip_mbid_sasaran` sasaranbid
	where
		sasaranbid.Kd_Pemda = $KdPemda
		and sasaranbid.Kd_Urusan = opd.Kd_Urusan
		and sasaranbid.Kd_Bidang = opd.Kd_Bidang
		and sasaranbid.Kd_Unit = opd.Kd_Unit
		and sasaranbid.Kd_Sub = opd.Kd_Sub
) as Cascading_Es3,
(
	select count(*) from `sakip_msubbid_sasaran` sasaransub
	where
		sasaransub.Kd_Pemda = $KdPemda
		and sasaransub.Kd_Urusan = opd.Kd_Urusan
		and sasaransub.Kd_Bidang = opd.Kd_Bidang
		and sasaransub.Kd_Unit = opd.Kd_Unit
		and sasaransub.Kd_Sub = opd.Kd_Sub
) as Cascading_Es4,
(
	select count(*) from `sakip_dpa_program` dpaprog
	where
		dpaprog.Kd_Pemda = $KdPemda
		and dpaprog.Kd_Tahun = $Tahun
		and dpaprog.Kd_Urusan = opd.Kd_Urusan
		and dpaprog.Kd_Bidang = opd.Kd_Bidang
		and dpaprog.Kd_Unit = opd.Kd_Unit
		and dpaprog.Kd_Sub = opd.Kd_Sub
) as DPA_Prg,
(
	select count(*) from `sakip_dpa_kegiatan` dpakeg
	where
		dpakeg.Kd_Pemda = $KdPemda
		and dpakeg.Kd_Tahun = $Tahun
		and dpakeg.Kd_Urusan = opd.Kd_Urusan
		and dpakeg.Kd_Bidang = opd.Kd_Bidang
		and dpakeg.Kd_Unit = opd.Kd_Unit
		and dpakeg.Kd_Sub = opd.Kd_Sub
) as DPA_Keg
from ref_sub_unit opd
order by opd.Kd_Urusan, opd.Kd_Unit, opd.Kd_Bidang, opd.Kd_Sub, opd.Nm_Sub_Unit asc
        ";
        $data['res'] = $this->db->query($q)->result_array();
        $this->template->load('frontend' , 'home/status_opd', $data);
      }
  }
}
 ?>
