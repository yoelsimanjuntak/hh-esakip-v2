<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ ] . '" />',
      anchor('ajbk/master/option-edit/'.$d[COL_UNIQ],$d[COL_OPT_NAME],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_OPT_NAME], 'data-desc'=>$d[COL_OPT_DESC])),
      $d[COL_OPT_DESC]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
            <?=anchor('ajbk/master/option-delete','<i class="fa fa-trash"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('ajbk/master/option-add','<i class="fa fa-plus"></i> CREATE',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
        </p>

        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor" method="post" action="#">
          <div class="form-group">
              <label>Opsi</label>
              <div class="input-group">
                <input type="hidden" name="<?=COL_OPT_CODE?>" value="<?=$optcode?>" required />
                <input type="text" class="form-control" name="<?=COL_OPT_NAME?>" required />
              </div>
          </div>
          <div class="form-group">
              <label>Deskripsi</label>
              <div class="input-group">
                <textarea class="form-control" rows="2" name="<?=COL_OPT_DESC?>"></textarea>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-flat btn-ok">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth" : false,
      //"sDom": "Rlfrtip",
      "aaData": <?=$data?>,
      //"bJQueryUI": true,
      //"aaSorting" : [[5,'desc']],
      "scrollY" : '40vh',
      "scrollX": "120%",
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
      "order": [[ 1, "asc" ]],
      "aoColumns": [
          {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" />", "width": "10px","bSortable":false},
          {"sTitle": "Opsi"},
          {"sTitle": "Deskripsi"}
      ]
    });
    $('#cekbox').click(function(){
      if($(this).is(':checked')){
        $('.cekbox').prop('checked',true);
      }else{
        $('.cekbox').prop('checked',false);
      }
    });

    $('.modal-popup, .modal-popup-edit').click(function(){
      var a = $(this);
      var name = $(this).data('name');
      var desc = $(this).data('desc');
      var editor = $("#modal-editor");

      $('[name=<?=COL_OPT_NAME?>]', editor).val(name);
      $('[name=<?=COL_OPT_DESC?>]', editor).val(desc);
      editor.modal("show");
      $(".btn-ok", editor).unbind('click').click(function() {
        var dis = $(this);
        dis.html("Loading...").attr("disabled", true);
        $('#form-editor').ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
                window.location.reload();
            }else{
                toastr.error(data.error);
            }
          },
          complete: function() {
            dis.html("Submit").attr("disabled", false);
          }
        });
      });
      return false;
    });
});
</script>
