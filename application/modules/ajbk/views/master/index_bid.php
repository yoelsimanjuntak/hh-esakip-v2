<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 17:27
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $nmSub = "";
    $this->db->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
    $this->db->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
    $this->db->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
    $this->db->where(COL_KD_SUB, $d[COL_KD_SUB]);
    $subunit = $this->db->get("ref_sub_unit")->row_array();
    if($subunit) {
        $nmSub = $subunit["Nm_Sub_Unit"];
    }

    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
        anchor('ajbk/master/bid-edit/'.$d["ID"],$d[COL_KD_BID].". ".$d[COL_NM_BID]),
        $nmSub
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
            <?=anchor('ajbk/master/bid-del','<i class="fa fa-trash"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('ajbk/master/bid-add','<i class="fa fa-plus"></i> CREATE',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
        </p>

        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
          "autoWidth" : false,
          //"sDom": "Rlfrtip",
          "aaData": <?=$data?>,
          //"bJQueryUI": true,
          //"aaSorting" : [[5,'desc']],
          "scrollY" : '40vh',
          "scrollX": "120%",
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
          //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
          "order": [[ 1, "asc" ]],
          "aoColumns": [
              {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" />", "width": "10px","bSortable":false},
              {"sTitle": "Unit Kerja Administrator"},
              {"sTitle": "Unit Kerja Pratama"},

          ]
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
                console.log('clicked');
            }else{
                $('.cekbox').prop('checked',false);
            }
        });
    });
</script>
