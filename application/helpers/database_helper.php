<?php
define('TBL__ROLES','_roles');
define('TBL__SETTINGS','_settings');
define('TBL__USERINFORMATION','_userinformation');
define('TBL__USERS','_users');
define('TBL_AJBK_JABATAN','ajbk_jabatan');
define('TBL_AJBK_JABATAN_BEZETTING','ajbk_jabatan_bezetting');
define('TBL_AJBK_JABATAN_DIKLAT','ajbk_jabatan_diklat');
define('TBL_AJBK_JABATAN_JURUSAN','ajbk_jabatan_jurusan');
define('TBL_AJBK_JABATAN_URAIAN','ajbk_jabatan_uraian');
define('TBL_AJBK_NOMENKLATUR','ajbk_nomenklatur');
define('TBL_AJBK_OPTION','ajbk_option');
define('TBL_AJBK_UNIT','ajbk_unit');
define('TBL_AJBK_UNIT_BID','ajbk_unit_bid');
define('TBL_AJBK_UNIT_SUBBID','ajbk_unit_subbid');
define('TBL_REF_SUB_UNIT','ref_sub_unit');
define('TBL_SAKIP_DPA_KEGIATAN','sakip_dpa_kegiatan');
define('TBL_SAKIP_DPA_KEGIATAN_INDIKATOR','sakip_dpa_kegiatan_indikator');
define('TBL_SAKIP_DPA_KEGIATAN_SASARAN','sakip_dpa_kegiatan_sasaran');
define('TBL_SAKIP_DPA_PROGRAM','sakip_dpa_program');
define('TBL_SAKIP_DPA_PROGRAM_INDIKATOR','sakip_dpa_program_indikator');
define('TBL_SAKIP_DPA_PROGRAM_SASARAN','sakip_dpa_program_sasaran');
define('TBL_SAKIP_INDIVIDU_INDIKATOR','sakip_individu_indikator');
define('TBL_SAKIP_INDIVIDU_SASARAN','sakip_individu_sasaran');
define('TBL_SAKIP_MBID','sakip_mbid');
define('TBL_SAKIP_MBID_INDIKATOR','sakip_mbid_indikator');
define('TBL_SAKIP_MBID_PROGRAM','sakip_mbid_program');
define('TBL_SAKIP_MBID_PROGRAM_INDIKATOR','sakip_mbid_program_indikator');
define('TBL_SAKIP_MBID_PROGRAM_SASARAN','sakip_mbid_program_sasaran');
define('TBL_SAKIP_MBID_SASARAN','sakip_mbid_sasaran');
define('TBL_SAKIP_MOPD_FILE','sakip_mopd_file');
define('TBL_SAKIP_MOPD_IKSASARAN','sakip_mopd_iksasaran');
define('TBL_SAKIP_MOPD_IKTUJUAN','sakip_mopd_iktujuan');
define('TBL_SAKIP_MOPD_SASARAN','sakip_mopd_sasaran');
define('TBL_SAKIP_MOPD_TUJUAN','sakip_mopd_tujuan');
define('TBL_SAKIP_MPEMDA','sakip_mpemda');
define('TBL_SAKIP_MPMD_IKSASARAN','sakip_mpmd_iksasaran');
define('TBL_SAKIP_MPMD_IKTUJUAN','sakip_mpmd_iktujuan');
define('TBL_SAKIP_MPMD_MISI','sakip_mpmd_misi');
define('TBL_SAKIP_MPMD_SASARAN','sakip_mpmd_sasaran');
define('TBL_SAKIP_MPMD_TUJUAN','sakip_mpmd_tujuan');
define('TBL_SAKIP_MSATUAN','sakip_msatuan');
define('TBL_SAKIP_MSUBBID','sakip_msubbid');
define('TBL_SAKIP_MSUBBID_INDIKATOR','sakip_msubbid_indikator');
define('TBL_SAKIP_MSUBBID_KEGIATAN','sakip_msubbid_kegiatan');
define('TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR','sakip_msubbid_kegiatan_indikator');
define('TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN','sakip_msubbid_kegiatan_sasaran');
define('TBL_SAKIP_MSUBBID_SASARAN','sakip_msubbid_sasaran');
define('TBL_SAKIP_MSUMBERDANA','sakip_msumberdana');
define('TBL_SAKIP_PDPA_KEGIATAN','sakip_pdpa_kegiatan');
define('TBL_SAKIP_PDPA_KEGIATAN_INDIKATOR','sakip_pdpa_kegiatan_indikator');
define('TBL_SAKIP_PDPA_KEGIATAN_SASARAN','sakip_pdpa_kegiatan_sasaran');
define('TBL_SAKIP_PDPA_PROGRAM','sakip_pdpa_program');
define('TBL_SAKIP_PDPA_PROGRAM_INDIKATOR','sakip_pdpa_program_indikator');
define('TBL_SAKIP_PDPA_PROGRAM_SASARAN','sakip_pdpa_program_sasaran');
define('TBL_SAKIP_PRENJA_KEGIATAN','sakip_prenja_kegiatan');
define('TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR','sakip_prenja_kegiatan_indikator');
define('TBL_SAKIP_PRENJA_KEGIATAN_SASARAN','sakip_prenja_kegiatan_sasaran');
define('TBL_SAKIP_PRENJA_PROGRAM','sakip_prenja_program');
define('TBL_SAKIP_PRENJA_PROGRAM_INDIKATOR','sakip_prenja_program_indikator');
define('TBL_SAKIP_PRENJA_PROGRAM_SASARAN','sakip_prenja_program_sasaran');
define('TBL_TA_KEGIATAN','ta_kegiatan');
define('TBL_TA_PROGRAM','ta_program');

define('COL_ROLEID','RoleID');
define('COL_ROLENAME','RoleName');
define('COL_SETTINGID','SettingID');
define('COL_SETTINGLABEL','SettingLabel');
define('COL_SETTINGNAME','SettingName');
define('COL_SETTINGVALUE','SettingValue');
define('COL_USERNAME','UserName');
define('COL_EMAIL','Email');
define('COL_COMPANYID','CompanyID');
define('COL_NAME','Name');
define('COL_IDENTITYNO','IdentityNo');
define('COL_BIRTHDATE','BirthDate');
define('COL_RELIGIONID','ReligionID');
define('COL_GENDER','Gender');
define('COL_ADDRESS','Address');
define('COL_PHONENUMBER','PhoneNumber');
define('COL_EDUCATIONID','EducationID');
define('COL_UNIVERSITYNAME','UniversityName');
define('COL_FACULTYNAME','FacultyName');
define('COL_MAJORNAME','MajorName');
define('COL_ISGRADUATED','IsGraduated');
define('COL_GRADUATEDDATE','GraduatedDate');
define('COL_YEAROFEXPERIENCE','YearOfExperience');
define('COL_RECENTPOSITION','RecentPosition');
define('COL_RECENTSALARY','RecentSalary');
define('COL_EXPECTEDSALARY','ExpectedSalary');
define('COL_CVFILENAME','CVFilename');
define('COL_IMAGEFILENAME','ImageFilename');
define('COL_REGISTEREDDATE','RegisteredDate');
define('COL_PASSWORD','Password');
define('COL_ISSUSPEND','IsSuspend');
define('COL_LASTLOGIN','LastLogin');
define('COL_LASTLOGINIP','LastLoginIP');
define('COL_KD_JABATAN','Kd_Jabatan');
define('COL_KD_NOMENKLATUR','Kd_Nomenklatur');
define('COL_NM_JABATAN','Nm_Jabatan');
define('COL_ID_JABATAN','Id_Jabatan');
define('COL_KD_URUSAN','Kd_Urusan');
define('COL_KD_BIDANG','Kd_Bidang');
define('COL_KD_UNIT','Kd_Unit');
define('COL_KD_SUB','Kd_Sub');
define('COL_KD_BID','Kd_Bid');
define('COL_KD_SUBBID','Kd_Subbid');
define('COL_KD_TYPE','Kd_Type');
define('COL_NM_IKHTISAR','Nm_Ikhtisar');
define('COL_KD_PENDIDIKAN','Kd_Pendidikan');
define('COL_KD_DIKLAT','Kd_Diklat');
define('COL_KD_PENGALAMAN','Kd_Pengalaman');
define('COL_NM_BAHANKERJA','Nm_BahanKerja');
define('COL_NM_PERANGKATKERJA','Nm_PerangkatKerja');
define('COL_NM_TANGGUNGJAWAB','Nm_TanggungJawab');
define('COL_NM_WEWENANG','Nm_Wewenang');
define('COL_NM_KORELASI','Nm_Korelasi');
define('COL_NM_KONDISI','Nm_Kondisi');
define('COL_NM_RESIKO','Nm_Resiko');
define('COL_NM_KETERAMPILAN','Nm_Keterampilan');
define('COL_NM_BAKAT','Nm_Bakat');
define('COL_NM_TEMPRAMEN','Nm_Tempramen');
define('COL_NM_MINAT','Nm_Minat');
define('COL_NM_UPAYAFISIK','Nm_UpayaFisik');
define('COL_NM_KONDISIFISIK','Nm_KondisiFisik');
define('COL_NM_FUNGSIPEKERJAAN','Nm_FungsiPekerjaan');
define('COL_CREATE_BY','Create_By');
define('COL_CREATE_DATE','Create_Date');
define('COL_EDIT_DATE','Edit_Date');
define('COL_EDIT_BY','Edit_By');
define('COL_UNIQ','Uniq');
define('COL_TAHUN','Tahun');
define('COL_JLH_PEGAWAI','Jlh_Pegawai');
define('COL_KD_JURUSAN','Kd_Jurusan');
define('COL_NM_URAIAN','Nm_Uraian');
define('COL_KD_SATUAN','Kd_Satuan');
define('COL_JLH_BEBAN','Jlh_Beban');
define('COL_JLH_JAM','Jlh_Jam');
define('COL_NM_NOMENKLATUR','Nm_Nomenklatur');
define('COL_OPT_CODE','Opt_Code');
define('COL_OPT_NAME','Opt_Name');
define('COL_OPT_DESC','Opt_Desc');
define('COL_NM_SUB_UNIT','Nm_Sub_Unit');
define('COL_NM_BID','Nm_Bid');
define('COL_NM_SUBBID','Nm_Subbid');
define('COL_NM_PIMPINAN','Nm_Pimpinan');
define('COL_NM_KOP1','Nm_Kop1');
define('COL_NM_KOP2','Nm_Kop2');
define('COL_KD_PEMDA','Kd_Pemda');
define('COL_KD_MISI','Kd_Misi');
define('COL_KD_TUJUAN','Kd_Tujuan');
define('COL_KD_INDIKATORTUJUAN','Kd_IndikatorTujuan');
define('COL_KD_SASARAN','Kd_Sasaran');
define('COL_KD_INDIKATORSASARAN','Kd_IndikatorSasaran');
define('COL_KD_TAHUN','Kd_Tahun');
define('COL_KD_TUJUANOPD','Kd_TujuanOPD');
define('COL_KD_INDIKATORTUJUANOPD','Kd_IndikatorTujuanOPD');
define('COL_KD_SASARANOPD','Kd_SasaranOPD');
define('COL_KD_INDIKATORSASARANOPD','Kd_IndikatorSasaranOPD');
define('COL_KD_PROGRAMOPD','Kd_ProgramOPD');
define('COL_KD_SASARANPROGRAMOPD','Kd_SasaranProgramOPD');
define('COL_KD_KEGIATANOPD','Kd_KegiatanOPD');
define('COL_NM_KEGIATANOPD','Nm_KegiatanOPD');
define('COL_IS_RENJA','Is_Renja');
define('COL_TOTAL','Total');
define('COL_BUDGET','Budget');
define('COL_PERGESERAN','Pergeseran');
define('COL_BUDGET_TW1','Budget_TW1');
define('COL_BUDGET_TW2','Budget_TW2');
define('COL_BUDGET_TW3','Budget_TW3');
define('COL_BUDGET_TW4','Budget_TW4');
define('COL_ANGGARAN_TW1','Anggaran_TW1');
define('COL_ANGGARAN_TW2','Anggaran_TW2');
define('COL_ANGGARAN_TW3','Anggaran_TW3');
define('COL_ANGGARAN_TW4','Anggaran_TW4');
define('COL_KD_SUMBERDANA','Kd_SumberDana');
define('COL_KD_SASARANKEGIATANOPD','Kd_SasaranKegiatanOPD');
define('COL_KD_INDIKATORKEGIATANOPD','Kd_IndikatorKegiatanOPD');
define('COL_NM_INDIKATORKEGIATANOPD','Nm_IndikatorKegiatanOPD');
define('COL_NM_FORMULA','Nm_Formula');
define('COL_NM_SUMBERDATA','Nm_SumberData');
define('COL_NM_PENANGGUNGJAWAB','Nm_PenanggungJawab');
define('COL_TARGET','Target');
define('COL_TARGET_TW1','Target_TW1');
define('COL_TARGET_TW2','Target_TW2');
define('COL_TARGET_TW3','Target_TW3');
define('COL_TARGET_TW4','Target_TW4');
define('COL_KINERJA_TW1','Kinerja_TW1');
define('COL_KINERJA_TW2','Kinerja_TW2');
define('COL_KINERJA_TW3','Kinerja_TW3');
define('COL_KINERJA_TW4','Kinerja_TW4');
define('COL_NM_SASARANKEGIATANOPD','Nm_SasaranKegiatanOPD');
define('COL_NM_PROGRAMOPD','Nm_ProgramOPD');
define('COL_KD_INDIKATORPROGRAMOPD','Kd_IndikatorProgramOPD');
define('COL_NM_INDIKATORPROGRAMOPD','Nm_IndikatorProgramOPD');
define('COL_NM_SASARANPROGRAMOPD','Nm_SasaranProgramOPD');
define('COL_KD_SASARANSUBBIDANG','Kd_SasaranSubbidang');
define('COL_KD_SASARANINDIVIDU','Kd_SasaranIndividu');
define('COL_KD_INDIKATORINDIVIDU','Kd_IndikatorIndividu');
define('COL_NM_SASARANINDIVIDU','Nm_SasaranIndividu');
define('COL_NM_INDIKATORINDIVIDU','Nm_IndikatorIndividu');
define('COL_NM_TARGET','Nm_Target');
define('COL_NM_PEGAWAI','Nm_Pegawai');
define('COL_NM_KABID','Nm_Kabid');
define('COL_NM_SASARANPROGRAM','Nm_SasaranProgram');
define('COL_NM_INDIKATORPROGRAM','Nm_IndikatorProgram');
define('COL_AWAL','Awal');
define('COL_TARGET_N1','Target_N1');
define('COL_ISEPLAN','IsEplan');
define('COL_REMARKS','Remarks');
define('COL_AKHIR','Akhir');
define('COL_NM_KETERANGAN','Nm_Keterangan');
define('COL_NM_FILE','Nm_File');
define('COL_NM_INDIKATORSASARANOPD','Nm_IndikatorSasaranOPD');
define('COL_NM_INDIKATORTUJUANOPD','Nm_IndikatorTujuanOPD');
define('COL_NM_SASARANOPD','Nm_SasaranOPD');
define('COL_NM_TUJUANOPD','Nm_TujuanOPD');
define('COL_KD_PROV','Kd_Prov');
define('COL_KD_KAB','Kd_Kab');
define('COL_KD_TAHUN_FROM','Kd_Tahun_From');
define('COL_KD_TAHUN_TO','Kd_Tahun_To');
define('COL_NM_KAB','Nm_Kab');
define('COL_NM_PEJABAT','Nm_Pejabat');
define('COL_NM_POSISI','Nm_Posisi');
define('COL_NM_VISI','Nm_Visi');
define('COL_NM_ALAMAT_KAB','Nm_Alamat_Kab');
define('COL_NM_KET_PERIODE','Nm_Ket_Periode');
define('COL_NM_INDIKATORSASARAN','Nm_IndikatorSasaran');
define('COL_NM_INDIKATORTUJUAN','Nm_IndikatorTujuan');
define('COL_NM_MISI','Nm_Misi');
define('COL_NM_SASARAN','Nm_Sasaran');
define('COL_NM_TUJUAN','Nm_Tujuan');
define('COL_NM_SATUAN','Nm_Satuan');
define('COL_NM_KASUBBID','Nm_Kasubbid');
define('COL_KD_INDIKATORSUBBIDANG','Kd_IndikatorSubbidang');
define('COL_NM_INDIKATORSUBBIDANG','Nm_IndikatorSubbidang');
define('COL_TOTAL_N1','Total_N1');
define('COL_NM_SASARANSUBBIDANG','Nm_SasaranSubbidang');
define('COL_NM_SUMBERDANA','Nm_SumberDana');
define('COL_KD_PROG','Kd_Prog');
define('COL_KD_KEG','Kd_Keg');
define('COL_ID_PROG','ID_Prog');
define('COL_KET_KEGIATAN','Ket_Kegiatan');
define('COL_LOKASI','Lokasi');
define('COL_KELOMPOK_SASARAN','Kelompok_Sasaran');
define('COL_STATUS_KEGIATAN','Status_Kegiatan');
define('COL_PAGU_ANGGARAN','Pagu_Anggaran');
define('COL_WAKTU_PELAKSANAAN','Waktu_Pelaksanaan');
define('COL_KD_SUMBER','Kd_Sumber');
define('COL_STATUS','Status');
define('COL_KETERANGAN','Keterangan');
define('COL_PAGU_ANGGARAN_NT1','Pagu_Anggaran_Nt1');
define('COL_VERIFIKASI_BAPPEDA','Verifikasi_Bappeda');
define('COL_TANGGAL_VERIFIKASI_BAPPEDA','Tanggal_Verifikasi_Bappeda');
define('COL_KETERANGAN_VERIFIKASI_BAPPEDA','Keterangan_Verifikasi_Bappeda');
define('COL_KD_REF','Kd_Ref');
define('COL_KET_PROG','Ket_Prog');
define('COL_TOLAK_UKUR','Tolak_Ukur');
define('COL_TARGET_ANGKA','Target_Angka');
define('COL_TARGET_URAIAN','Target_Uraian');
define('COL_KD_URUSAN1','Kd_Urusan1');
define('COL_KD_BIDANG1','Kd_Bidang1');
