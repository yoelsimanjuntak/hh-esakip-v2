<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 8/9/2019
 * Time: 10:43 PM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - Laporan DPA.xls");
}
?>
<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
        <caption>
            <h5 style="text-decoration: underline"><?="REALISASI ANGGARAN TAHUN ".$data[COL_KD_TAHUN]?></h5>
        </caption>
        <thead>
        <tr>
            <th>No.</th>
            <th>Sasaran</th>
            <th>Indikator Kinerja</th>
            <th>Satuan</th>
            <th>Target</th>
            <th colspan="2">Program</th>
            <th>Anggaran (Rp)</th>
            <th>Realisasi (Rp)</th>
            <th>% Capaian</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        $last = array(
            "UNIQ_SASARAN_OPD" => "@@",
            "UNIQ_IKSASARAN_OPD" => "@@"
        );
        foreach($program as $p) {
            $ikprogram = $this->db
                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                ->where(COL_KD_SUB, $p[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $p[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $p[COL_KD_SASARANPROGRAMOPD])
                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                ->result_array();

            $kegiatan_sum = $this->db
                ->select("SUM(Budget) as TotalBudget, SUM(Anggaran_TW1+Anggaran_TW2+Anggaran_TW3+Anggaran_TW4) as TotalAnggaran")
                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                ->where(COL_KD_SUB, $p[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $p[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $p[COL_KD_SASARANPROGRAMOPD])
                ->get(TBL_SAKIP_DPA_KEGIATAN)
                ->row_array();
            $kegiatan = $this->db
                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                ->where(COL_KD_SUB, $p[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $p[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $p[COL_KD_SASARANPROGRAMOPD])
                ->order_by(COL_KD_KEGIATANOPD, "asc")
                ->get(TBL_SAKIP_DPA_KEGIATAN)
                ->result_array();

            foreach($ikprogram as $ikp) {
                $cpprogram = !empty($kegiatan_sum) && $kegiatan_sum["TotalBudget"] > 0 ? ($kegiatan_sum["TotalAnggaran"] / $kegiatan_sum["TotalBudget"]) * 100 : 0;
                ?>
                <tr>
                    <?php
                    if($p["UNIQ_SASARAN_OPD"] != $last["UNIQ_SASARAN_OPD"]) {
                        ?>
                        <td <?=count($ikprogram) + count($kegiatan) > 1 ? 'rowspan="'.(count($ikprogram)+count($kegiatan)).'"' : '' ?> style="text-align: center"><?=$counter?></td>
                        <td <?=count($ikprogram) + count($kegiatan) > 1 ? 'rowspan="'.(count($ikprogram)+count($kegiatan)).'"' : '' ?>><?=$p[COL_NM_SASARANOPD]?></td>
                        <td <?=count($ikprogram) + count($kegiatan) > 1 ? 'rowspan="'.(count($ikprogram)+count($kegiatan)).'"' : '' ?>><?=$p[COL_NM_INDIKATORSASARANOPD]?></td>
                        <?php
                        $last["UNIQ_SASARAN_OPD"] = $p["UNIQ_SASARAN_OPD"];
                    }
                    ?>
                    <?php
                    if($p["UNIQ_IKSASARAN_OPD"] != $last["UNIQ_IKSASARAN_OPD"]) {
                        ?>

                        <?php
                        $last["UNIQ_IKSASARAN_OPD"] = $p["UNIQ_IKSASARAN_OPD"];
                    }
                    ?>


                    <td <?=count($kegiatan) > 0 ? 'rowspan="'.(count($kegiatan)+1).'"' : '' ?>><?=$ikp[COL_KD_SATUAN]?></td>
                    <td <?=count($kegiatan) > 0 ? 'rowspan="'.(count($kegiatan)+1).'"' : '' ?> style="text-align: right"><?=number_format($ikp[COL_TARGET], 2)?></td>
                    <td <?=count($ikprogram) > 1 ? 'rowspan="'.(count($ikprogram)).'"' : '' ?> colspan="2"><?=$p[COL_NM_PROGRAMOPD]?></td>
                    <td <?=count($ikprogram) > 1 ? 'rowspan="'.(count($ikprogram)).'"' : '' ?> style="text-align: right"><?=number_format($kegiatan_sum["TotalBudget"], 0)?></td>
                    <td <?=count($ikprogram) > 1 ? 'rowspan="'.(count($ikprogram)).'"' : '' ?> style="text-align: right"><?=number_format($kegiatan_sum["TotalAnggaran"], 0)?></td>
                    <td <?=count($ikprogram) > 1 ? 'rowspan="'.(count($ikprogram)).'"' : '' ?> style="text-align: right"><?=number_format($cpprogram, 2)?></td>
                </tr>
                <?php
            }
            $counterkeg = 1;
            foreach($kegiatan as $keg) {
                $cpkegiatan = $keg[COL_BUDGET] > 0 ? (($keg[COL_ANGGARAN_TW1]+$keg[COL_ANGGARAN_TW2]+$keg[COL_ANGGARAN_TW3]+$keg[COL_ANGGARAN_TW4]) / $keg[COL_BUDGET]) * 100 : 0;
                ?>
                <tr>
                    <td style="text-align: center"><?=$counterkeg?></td>
                    <td><?=$keg[COL_NM_KEGIATANOPD]?></td>
                    <td style="text-align: right"><?=number_format($keg[COL_BUDGET], 0)?></td>
                    <td style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW1]+$keg[COL_ANGGARAN_TW2]+$keg[COL_ANGGARAN_TW3]+$keg[COL_ANGGARAN_TW4], 0)?></td>
                    <td style="text-align: right"><?=number_format($cpkegiatan, 2)?></td>
                </tr>
            <?php
                $counterkeg++;
            }
            $counter++;
        }
        ?>
        </tbody>
    </table>
</div>